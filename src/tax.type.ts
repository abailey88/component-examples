export enum TaxType {
    EIN = 'EIN',
    SSN_ITIN = 'SSN/ITIN',
    FOREIGN = 'FOREIGN',
}