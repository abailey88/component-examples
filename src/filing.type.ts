export enum FilingType {
    INITIAL = 'INITIAL',
    CORRECT = 'CORRECT',
    UPDATE = 'UPDATE',
    NEW_EXEMPT = 'NEW_EXEMPT',
}