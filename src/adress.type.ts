export enum AddressType {
    BUSINESS = 'BUSINESS',
    RESIDENTIAL = 'RESIDENTIAL',
}