import { AddressType } from "../enum/address-type.enum";
import { FilingType } from "../enum/filing-type.enum";
import { TaxType } from "../enum/tax-type.enum";

interface FieldValue<T = string> {
    unknown?: boolean;
    value?: T;
}

interface TaxIdentification {
    taxType: TaxType;
    taxID: number;
    taxJurisdiction?: string;
}

interface Address {
    type?: FieldValue<AddressType>;
    address: FieldValue | string;
    city: FieldValue | string;
    country: FieldValue | string;
    state: FieldValue | string;
    zip: FieldValue<number> | number;
}

interface Person {
    firstName: FieldValue;
    lastName: FieldValue;
    middleName?: string;
    suffix?: string;
    dob: FieldValue;
}

interface JurisdictionFormation {
    state?: string;
    tribalJurisdiction?: string;
    tribalDescription?: string;
}

interface ReportCompanyFormation {
    jurisdiction: string;

    domestic: JurisdictionFormation;
    
    foreign: JurisdictionFormation;
}

interface IdentificationDocument {
    type: FieldValue;
    id: FieldValue<string | number>;

    jurisdiction: FieldValue;
    state: FieldValue;
    localTribal?: FieldValue;
    localTribalDescription?: string;

    imageId: FieldValue;
}

interface FilingInformation extends Omit<TaxIdentification, 'taxJurisdiction'> {
    type: FilingType;
    datePrepared: string;
    legalName?: string;
}

interface ReportingCompany extends TaxIdentification {
    requestID: boolean;
    isForeignPooledInvestmentVehicle: boolean;

    legalName: string;
    alternateNames: string[];

    address: Omit<Address, 'type'>;

    formation: ReportCompanyFormation;
}

interface CompanyApplicant {
    isExisitingReportingCompany?: boolean;
    isUnableToRecordAllCA?: boolean;

    fincenID?: number;

    person: Person;

    address: Address;

    identification: IdentificationDocument;
}

interface BeneficialOwner {
    isParentGuardianInformation?: boolean;
    isExemptEntity?: boolean;

    fincenID?: number;

    person: Person;

    address: Omit<Address, 'type'>;

    identification: IdentificationDocument;
}

export interface BOIRForm {
    ua: boolean;
    fi: FilingInformation;
    rc: ReportingCompany;
    ca: CompanyApplicant[];
    bo: BeneficialOwner[];
}