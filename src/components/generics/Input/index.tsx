interface IInput extends React.InputHTMLAttributes<HTMLInputElement> {
    error?: boolean;
    fill?: boolean;
}

export const Input = (props: IInput) => {
    const { error, fill, type, className, style: _style = {}, ...rest } = props;

    const classes = [
        'usa-input',
        className,
    ];

    const style: React.CSSProperties = {};

    if (error) {
        classes.push('usa-input--error');
    }

    if (fill) {
        style.width = '100%';
        style.maxWidth = '100%';
    }

    return (
        <input
            {...rest}
            style={style}
            className={classes.join(' ')}
        />
    )
}