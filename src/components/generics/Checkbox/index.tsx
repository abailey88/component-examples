interface ICheckbox extends React.InputHTMLAttributes<HTMLInputElement> {
    
}

export const Checkbox = (props: ICheckbox) => {
    const { className, children, ...rest } = props;

    const classes = [
        'usa-checkbox',
        className,
    ];

    return (
        <div className={classes.join(' ')} >
            <input
                {...rest}
                type='checkbox'
                className={'usa-checkbox__input'}
            />
            <label className="usa-checkbox__label" htmlFor={rest.id}>
                {children}
            </label>
        </div>
    )
}