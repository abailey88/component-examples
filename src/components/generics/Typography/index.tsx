type ElementTypes = |
    'label' | 
    'span' | 
    'h1' |
    'h2' |
    'h3' |
    'h4' |
    'h5' |
    'h6' 

type SizeType = |
    'xs' | 
    'sm' |
    'md' |
    'lg' |
    'xl' |
    '2xl' |
    '3xl' 

type TypographyType = |
    'primary' | 
    'success' |
    'error' |
    'secondary' |
    'warning'

interface ITypography extends React.HTMLAttributes<HTMLSpanElement> {
    as?: ElementTypes;
    size?: SizeType;
    bold?: boolean;
    type?: TypographyType;
    [x:string]: any;
}

const types = {
    primary: 'text-cyan',
    success: 'text-mint',
    error: 'text-red',
    warning: 'text-orange',
    secondary: 'text-gray-50',
}

const sizes = {
    'xs': 'font-ui-xs',
    'sm': 'font-ui-sm',
    'md': 'font-ui-md',
    'lg': 'font-ui-lg',
    'xl': 'font-ui-xl',
    '2xl': 'font-ui-2xl',
    '3xl': 'font-ui-3xl',
}

export const Typography = (props: ITypography) => {
    const { type, size, as, bold, children, className, ...rest } = props;

    let Element: any = 'span';
    const classes = [className]

    if (!!as) {
        Element = as;
    }
    
    if (size && sizes[size]) {
        classes.push(sizes[size]);
    }

    if (bold) {
        classes.push('text-bold');
    }

    if (type && types[type]) {
        classes.push(types[type])
    }

    return (
        <Element 
            className={classes.join(' ')}
            {...rest}
        >
            {children}
        </Element>
    )
}