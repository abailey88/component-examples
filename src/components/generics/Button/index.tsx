type ButtonType = |
    'primary' |
    'secondary' |
    'accent-cool' |
    'accent-warm' |
    'unstyled' |
    'base' |
    'outline' |
    'outline-inverse'

interface IButton extends React.HTMLAttributes<Omit<HTMLButtonElement, 'type'>> {
    active?: boolean;
    disabled?: boolean;
    type?: ButtonType;
    htmlType?: 'submit' | 'button';
    large?: boolean;
}

const types = {
    primary: 'usa-button--primary',
    secondary: 'usa-button--secondary',
    'accent-cool': 'usa-button--accent-cool',
    'accent-warm': 'usa-button--accent-warm',
    unstyled: 'usa-button--unstyled',
    base: 'usa-button--base',
    outline: 'usa-button--outline',
    'outline-inverse': 'usa-button--inverse',
}

export const Button = (props: IButton) => {
    const { large, type, active, htmlType = 'button', className, children, ...rest } = props;

    const classes = ['usa-button', className];

    if (type && types[type]) {
        classes.push(types[type]);
    }

    if (active) {
        classes.push('usa-button--active');
    }

    if (large) {
        classes.push('usa-button--big');
    }

    return (
        <button
            {...rest}
            className={classes.join(' ')}
            type={htmlType}
        >
            {children}
        </button>
    )
}