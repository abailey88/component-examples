import { Input } from "../Input"
import { Typography } from "../Typography";

interface IFormInput extends React.InputHTMLAttributes<HTMLInputElement> {
    label?: string;
    extra?: any;
    hint?: string;
    message?: any;
    required?: boolean;
    error?: boolean;
    fill?: boolean;
}

export const FormInput = (props: IFormInput) => {
    const { 
        label,
        message,
        hint,
        required,
        extra,
        error,
        className,
        style: _style = {},
        fill,
        ...rest
    } = props;

    const style: React.CSSProperties = {
        maxWidth: 320,
        ..._style,
    }

    if (fill) {
        style.width = '100%';
        style.maxWidth = '100%';
    }

    return (
        <div className={className} style={style}>
            <div className="flex space-between items-end">
                {!!label && (
                    <label 
                        className="usa-label"
                        htmlFor={rest.id}
                    >
                        {label}
                        {!!required && (
                            <span className="text-red">*</span>
                        )}
                    </label>
                )}
                {extra || (!!hint && (
                    <Typography 
                        className="usa-label"
                        size='xs'
                    >
                        {hint}
                    </Typography>
                ))}
            </div>
            <Input
                {...rest}
                fill={fill}
                error={error}
            />
            <div>
                {typeof message == 'string' ? (
                    <Typography
                        className="margin-top-1 display-block"
                        size='xs'
                        type={error ? 'error' : undefined}
                    >
                        {message}
                    </Typography>
                ) : message}
            </div>
        </div>
    )
}