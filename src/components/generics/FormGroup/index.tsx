import React from "react"

interface IFormGroup extends React.HTMLAttributes<HTMLFieldSetElement> {
    disabled?: boolean;
}

export const FormGroup = (props: IFormGroup) => {
    const { children, className, ...rest } = props;

    const classes = [className, 'usa-fieldset'];

    return (
        <fieldset
            className={classes.join(' ')}
            {...rest}
        >
            {children}
        </fieldset>
    )
}