import { Checkbox } from "../generics/Checkbox"
import { FormGroup } from "../generics/FormGroup"
import { FormInput } from "../generics/FormInput"

export const AddressBlock = () => {
    return (
        <FormGroup
            className="grid-container-widescreen"
        >
            <div
                className="grid-row grid-gap"
            >
                <div className="grid-col-12 tablet:grid-col-4 desktop:grid-col-6">
                    <FormInput
                        label='1. Street'
                        extra={<Checkbox>Unknown</Checkbox>}
                        fill
                        // Send hook form register here {...register('street')}
                    />
                </div>
                <div className="grid-col-12 tablet:grid-col-4 desktop:grid-col-3">
                    <FormInput
                        label='2. City'
                        extra={<Checkbox>Unknown</Checkbox>}
                        fill
                    />
                </div>
                <div className="grid-col-12 tablet:grid-col-4 desktop:grid-col-3">
                    <FormInput
                        label='3. State'
                        extra={<Checkbox>Unknown</Checkbox>}
                        fill
                    />
                </div>
            </div>
        </FormGroup>
    )
}