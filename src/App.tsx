import { FormGroup } from "./components/generics/FormGroup";
import { FormInput } from "./components/generics/FormInput";
import { Button } from "./components/generics/Button";
import { Checkbox } from "./components/generics/Checkbox";
import { Input } from "./components/generics/Input"
import { Typography } from "./components/generics/Typography"
import { AddressBlock } from "./components/AddressBlock";

const TYPOGRAPHY_TEXT = 'Lorem ipsum dolor sit amet';

export const App = () => {
    return (
        <div className="page-container">
            <section>
                <Typography as='h1' bold>Typography</Typography>
                <Typography size='xs'>{TYPOGRAPHY_TEXT}</Typography>
                <Typography size='sm'>{TYPOGRAPHY_TEXT}</Typography>
                <Typography size='md'>{TYPOGRAPHY_TEXT}</Typography>
                <Typography size='lg'>{TYPOGRAPHY_TEXT}</Typography>
                <Typography size='xl'>{TYPOGRAPHY_TEXT}</Typography>
                <Typography size='2xl'>{TYPOGRAPHY_TEXT}</Typography>
                <Typography size='3xl'>{TYPOGRAPHY_TEXT}</Typography>
            </section>
            <section>
                <Typography as='h1' bold>Buttons</Typography>
                <div className="flex space-x">
                    <Button>Button</Button>
                    <Button active>Button</Button>
                    <Button disabled>Button</Button>
                </div>
                <div className="flex space-x">
                    <Button type='accent-cool'>Button</Button>
                    <Button type='accent-cool' active>Button</Button>
                    <Button type='accent-cool' disabled>Button</Button>
                </div>
                <div className="flex space-x">
                    <Button type='accent-warm'>Button</Button>
                    <Button type='accent-warm'>Button</Button>
                    <Button type='accent-warm' active>Button</Button>
                    <Button type='accent-warm' disabled>Button</Button>
                </div>
                <div className="flex space-x">
                    <Button large>Button</Button>
                    <Button large active>Button</Button>
                    <Button large disabled>Button</Button>
                </div>
            </section>
            <section>
                <Typography as='h1' bold>Text Input</Typography>
                <Input placeholder='Ready for input'/>
                <Input error placeholder="Oh no!"/>
                <Input disabled placeholder="Disabled Input"/>
            </section>
            <section>
                <Typography as='h1' bold>Checkbox</Typography>
                <Checkbox defaultChecked id='default_checked'>Default Checked</Checkbox>
                <Checkbox id='checked'>Unchecked</Checkbox>
                <Checkbox disabled id='disabled'>Disabled</Checkbox>
            </section>
            <section>
                <Typography as='h1' bold>Form Input</Typography>
                <FormInput
                    label='1. First Name'
                    required
                    extra={<Checkbox>Unknown</Checkbox>}
                    error
                    fill
                    message={'Could be error data'}
                />
            </section>
            <section>
                <Typography as='h1' bold>Form Groups</Typography>
                <FormGroup
                    className="flex space-x"                    
                >
                    <FormInput
                        label='1. First Name'
                        required
                        extra={<Checkbox>Unknown</Checkbox>}
                        fill
                        message={'Could be error data'}
                    />
                    <FormInput
                        label='2. Last Name'
                        required
                        extra={<Checkbox>Unknown</Checkbox>}
                        fill
                        message={'Could be success data'}
                    />
                </FormGroup>
                <FormGroup
                    className="flex space-x"
                    disabled
                >
                    <FormInput
                        label='3. Account Number'
                        extra={<Checkbox>Unknown</Checkbox>}
                        fill
                    />
                    <FormInput
                        label='4. Routing Number'
                        extra={<Checkbox>Unknown</Checkbox>}
                        fill
                    />
                </FormGroup>
            </section>
            <section>
                <Typography as='h1' bold>Address Block (Responsive)</Typography>
                <AddressBlock/>
            </section>
        </div>
    )
}